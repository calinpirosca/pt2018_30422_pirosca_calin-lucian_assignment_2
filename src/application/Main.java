package application;
	
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;


public class Main extends Application {
	@Override
	public void start(Stage primaryStage) throws IOException {
		primaryStage.setTitle("Queue Simulation");
		primaryStage.initStyle(StageStyle.UNDECORATED); //to remove minimize,maximize,close button
		Parent root = FXMLLoader.load(getClass().getResource("/View/Main.fxml"));
		Scene scene = new Scene(root);
		primaryStage.setResizable(false);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	public static void main(String[] args) {
		launch(args);
	}
	
	public static void writeLogs(String message) { // writing logs to a file in the same directory as the executable

		Logger logger = Logger.getLogger("MyLog");
		FileHandler fh;
		logger.setUseParentHandlers(false); // display only to log file, not to console too.

		try {
			// configure the logger with handler and formatter
			fh = new FileHandler("MyLog.txt", true); // true to append
			logger.addHandler(fh);
			SimpleFormatter formatter = new SimpleFormatter();
			fh.setFormatter(formatter);

			// log the message
			logger.info(message);
			fh.close();

		} catch (SecurityException | IOException e) { // displaying info related to the file on the console
			e.printStackTrace();
		}
	}
}
