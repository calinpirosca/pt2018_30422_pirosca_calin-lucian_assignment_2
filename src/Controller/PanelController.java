package Controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;

import application.Main;
import javafx.fxml.FXML;

public class PanelController {

	@FXML
	JFXTextField qNr, intArr, intServ, startInterval, endInterval;
	// the intervals can be specified as follows : min-max. (with dash)
	// number - number, min to the left, max to the right
	// the date has to be specified as follows : start dd/mm/yyyy hour:min -
	// dd/mm/yyyy hour:min end

	@FXML
	JFXButton start, addQ, addC;

	int queues, minService, maxService, minIncoming, maxIncoming;
	Date startDate, endDate;
	
	Controller main;
		
	public void setMain(Controller main) { // used for communication with the main window
		this.main = main;
	}
	
	@FXML
	private void startSimulation() { // validation of input configuration
		String aux = qNr.getText();
		if (aux.matches("[0-9]+")) { // check the queue number
			queues = Integer.valueOf(aux);
			if (checkInterval(intArr, 0) == true) { // 0 for service
				if (checkInterval(intServ, 1) == true) { // 1 for incoming
					startDate = checkDate(startInterval);
					if (startDate != null) {
						endDate = checkDate(endInterval);
						if (endDate != null) {
							Main.writeLogs("queues = " + queues);
							Main.writeLogs("arriving interval " + minIncoming + "-" + maxIncoming);
							Main.writeLogs("service interval " + minService + "-" + maxService);
							Main.writeLogs("start date = " + startDate);
							Main.writeLogs("end date = " + endDate);
							long diff = endDate.getTime() - startDate.getTime();
							Main.writeLogs("Simulate for : " + TimeUnit.MILLISECONDS.toSeconds(diff));
							main.loadConfiguration(queues, minService, maxService, minIncoming, maxIncoming, TimeUnit.MILLISECONDS.toSeconds(diff));
						}
					}
				}
			}
		} else {
			qNr.setText("");
			qNr.setPromptText("Only numbers for queues amount !");
		}
	}

	@FXML
	private void addQueue() {
		main.addQueue();
		Main.writeLogs("Adaug queue");
	}

	@FXML
	private void addCustomer() {
		main.addCustomer();
		Main.writeLogs("Adaug customer");
	}

	private boolean checkInterval(JFXTextField interval, int option) {
		String aux = interval.getText();
		int min, max;
		if (aux.matches("[0-9]+\\-[0-9]+")) { // check the incoming interval of customers
			min = Integer.valueOf(aux.split("-")[0]);
			max = Integer.valueOf(aux.split("-")[1]);
			if (min > max) {
				interval.setText("");
				interval.setPromptText("Min must be <= than max");
				return false;
			}
			switch (option) {
			case 0:
				minIncoming = min;
				maxIncoming = max;
				break;
			case 1:
				minService = min;
				maxService = max;
				break;
			}
			return true;
		} else {
			interval.setText("");
			interval.setPromptText("Min-Max format accepted only for arriving interval!");
			return false;
		}
	}

	private Date checkDate(JFXTextField interval) {
		Date temp = null;
		try {
			temp = new SimpleDateFormat("dd/MM/yy hh:mm:ss", Locale.ENGLISH).parse(interval.getText());
		} catch (ParseException e) {
			interval.setText("");
			interval.setPromptText("dd/mm/yy hh:mm:ss format accepted only!");
		}
		return temp;
	}
}
