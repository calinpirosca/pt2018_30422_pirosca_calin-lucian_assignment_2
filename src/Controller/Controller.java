package Controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import application.Main;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Controller {

	volatile List<Node> images = new ArrayList<Node>(); // front-end
	volatile List<Node> info = new ArrayList<Node>();
	volatile List<Node> progress = new ArrayList<Node>();
	Set<Integer> orderNrs = new HashSet<>();
	double hboxWidth = 0;
	@FXML
	Button controlPanel;

	@FXML
	ProgressBar queueSimulation,nextCustomer;

	@FXML
	ScrollPane sp;

	@FXML
	AnchorPane ap;

	@FXML
	HBox image, information, progressCustomer; // first used for pictures, second for info
	
	@FXML
	ImageView movingCustomer;

	PanelController panel;

	Simulation simulation = new Simulation(this);

	ImageView customer = new ImageView();
	Text infos = new Text();

	@FXML
	public void initialize() {
		sp.setHbarPolicy(ScrollBarPolicy.AS_NEEDED);
		sp.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
		image.setAlignment(Pos.CENTER);
		images = image.getChildren();
		info = information.getChildren();
		progress = progressCustomer.getChildren();
	}
	
	public ImageView getMovingImage() {
		return movingCustomer;
	}

	private synchronized void reCenter() {
		if (ap.getPrefWidth() / 2 < hboxWidth / 2) {
			Platform.runLater(() -> {
				image.setLayoutX(40);
				information.setLayoutX(50);
				progressCustomer.setLayoutX(40);
			});
		} else {
			Platform.runLater(() -> {
				image.setLayoutX((ap.getPrefWidth() / 2 - hboxWidth / 2) - 10);
				information.setLayoutX((ap.getPrefWidth() / 2 - hboxWidth / 2) + 10);
				progressCustomer.setLayoutX((ap.getPrefWidth() / 2 - hboxWidth / 2) - 10);
			});
		}
	}
	
	public void addQueue() {
		simulation.addQueueu();
	}
	
	public void addCustomer() {
		simulation.addCustomer();
	}

	@FXML
	private void openPanel() {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/Panel.fxml"));
			Parent root = (Parent) loader.load();
			panel = loader.getController();
			panel.setMain(this);
			Stage controlStage = new Stage();
			Scene scene = new Scene(root);
			controlStage.setResizable(false);
			controlStage.setScene(scene);
			controlStage.setTitle("Control Panel");
			controlStage.show();
			controlStage.setX(controlPanel.getScene().getWindow().getX() + controlPanel.getScene().getWidth());
			controlStage.setY(controlPanel.getScene().getWindow().getY());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void loadConfiguration(int queues, int minS, int maxS, int minI, int maxI, long sim) {
		if (queues > 0) {
			simulation.loadConfiguration(queues, minS, maxS, minI, maxI, sim, nextCustomer,progress);
			startAnimation(sim);
		}
	}

	public void startAnimation(double time) {
		Timeline timeline = new Timeline();
		KeyValue keyValue1 = new KeyValue(queueSimulation.progressProperty(), 0.01);
		KeyFrame frame1 = new KeyFrame(Duration.millis(0), keyValue1);

		KeyValue keyValue2 = new KeyValue(queueSimulation.progressProperty(), 1);
		KeyFrame frame2 = new KeyFrame(Duration.seconds(time), keyValue2);

		timeline.getKeyFrames().addAll(frame1, frame2);
		timeline.play();
	}

	public synchronized void addCustomer(int orderNr, int currentSize, int currentServiceTime, int totalTime) {
		Main.writeLogs("Adaug in Queue " + (orderNr+1) + " are " + currentSize + " elemente");
		//System.out.print("in Queue " + orderNr + " are " + currentSize + " elemente,waiting time : ");
		switch (currentSize) {
		case 1:
			customer = new ImageView(getClass().getResource("/View/1man.png").toExternalForm());
			break;
		case 2:
			customer = new ImageView(getClass().getResource("/View/2man.png").toExternalForm());
			break;
		case 3:
			customer = new ImageView(getClass().getResource("/View/3man.png").toExternalForm());
			break;
		case 4:
			customer = new ImageView(getClass().getResource("/View/4man.png").toExternalForm());
			break;
		}
		infos = new Text("Queue " + orderNr + "\nSize = " + currentSize + "\nTime = " + currentServiceTime + "\nTotal = " + totalTime);
		if (orderNrs.contains(orderNr)) {
			Platform.runLater(() -> {
				images.set(orderNr, customer);
				info.set(orderNr,infos);
			});
		} else {
			hboxWidth = image.getWidth() + 80;
			Platform.runLater(() -> {
				images.add(customer);
				info.add(infos);
			});
			orderNrs.add(orderNr);
		}
		reCenter();
	}

	public synchronized void removeCustomer(int orderNr, int currentSize, int currentServiceTime, int totalTime) {
		Main.writeLogs("Sterg din Queue " + orderNr + " are " + (currentSize-1) + " elemente");
		//System.out.print("(sterg)Queue " + orderNr + " are " + (currentSize-1) + "elemente, waiting time ");
		switch (currentSize) {
		case 1:
			customer = new ImageView(getClass().getResource("/View/0man.png").toExternalForm());
			reCenter();
			break;
		case 2:
			customer = new ImageView(getClass().getResource("/View/1man.png").toExternalForm());
			reCenter();
			break;
		case 3:
			customer = new ImageView(getClass().getResource("/View/2man.png").toExternalForm());
			reCenter();
			break;
		case 4:
			customer = new ImageView(getClass().getResource("/View/3man.png").toExternalForm());
			reCenter();
			break;
		}
		infos = new Text("Queue " + orderNr + "\nSize = " + (currentSize-1) + "\nTime = " + currentServiceTime + "\nTotal = " + totalTime);
		Platform.runLater(() -> {
			images.set(orderNr, customer);
			info.set(orderNr, infos);
		});
	}
}
