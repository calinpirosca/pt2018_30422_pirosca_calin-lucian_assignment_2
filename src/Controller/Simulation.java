package Controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import Model.*;
import application.Main;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.scene.Node;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class Simulation {

	List<Node> progress = new ArrayList<Node>(); // used to store progressbars
	List<Threads> queueThreads = new ArrayList<Threads>(); // used to store the threads. Thread i belongs to queue i
	List<MyQueue> listqueue = new ArrayList<MyQueue>(); // used to store the qeueus. Queue i+1 belong to thread i+1

	// waitingTime
	int minService, maxService, minIncoming, maxIncoming, waitConsumer, serviceTime;
	long simulationTime;
	boolean endSimulation = false;

	Controller mainController; // used to communicate with the controller and get objects
	int i = 0;
	ProgressBar nextCustomer; // main progress bar, used to animate and display when the next customer will
								// pop up
	ImageView movingImage; // the image used to animate each time a customer leaves, set in place and
							// hidden until animated
	int peak = 0;
	long startTime, endTime; // used to calculate peak hour

	public Simulation(Controller main) { // establish communication with the controller
		mainController = main;
	}

	public void loadConfiguration(int queues, int minS, int maxS, int minI, int maxI, long sim,
			ProgressBar nextCustomer, List<Node> progress) {
		movingImage = mainController.getMovingImage(); // load what will be animated
		this.progress = progress; // load the list of progress bars that will be used
		while (queues > 0) { // load the queues
			listqueue.add(new MyQueue()); // add another queue
			progress.add(new ProgressBar()); // add another progress bar
			queueThreads.add(new Threads(listqueue.get(i), i, this, progress.get(i), movingImage)); // initialize a
																									// thread for each
			// queue
			queueThreads.get(i).start(); // starting all threads and use the methods inside the class to pause them when
											// necessary
			i++;
			queues--;
		}
		minService = minS;
		maxService = maxS;
		minIncoming = minI;
		maxIncoming = maxI;
		simulationTime = sim;
		this.nextCustomer = nextCustomer;
		startSimulation(sim); // start simulation of Main Progress bar
		getCustomers();
		startTime = System.nanoTime();
		trackCustomers();
	}

	public void trackCustomers() {
		Thread peakHour = new Thread(new Runnable() {

			@Override
			public void run() {
				while (endSimulation == false) {
					int temp = MyQueue.getTotalCustomers(listqueue);
					if (temp > peak) {
						peak = temp;
						endTime = System.nanoTime();
						try {
							Thread.sleep(500);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
			}
		});
		peakHour.start();
	}

	public void addQueueu() {
		listqueue.add(new MyQueue());
		progress.add(new ProgressBar());
		queueThreads.add(new Threads(listqueue.get(i), i, this, progress.get(i), movingImage));
		queueThreads.get(i).start();
		i++;
	}

	public void addCustomer() {
		serviceTime = getRandomNumber(minService, maxService); // how much to add to queue ( service time of customer )
		int orderNr = MyQueue.getBestQueue(listqueue); // where to add to queue ( least total waiting time )
		listqueue.get(orderNr).addToQueue(serviceTime); // adding to queue
		Main.writeLogs("Adaug (by addCustomer) " + serviceTime);
		// System.out.print("adaug " + serviceTime);
		mainController.addCustomer(orderNr, listqueue.get(orderNr).getQueueSize(),
				listqueue.get(orderNr).getCurrentServiceTime(), listqueue.get(orderNr).getTotalWaitingTime());
	}

	private void progressNextCustomer(double time) { // progress bar animation for "next customer"
		Timeline timeline = new Timeline();
		KeyValue keyValue1 = new KeyValue(nextCustomer.progressProperty(), 0.01);
		KeyFrame frame1 = new KeyFrame(Duration.millis(0), keyValue1);

		KeyValue keyValue2 = new KeyValue(nextCustomer.progressProperty(), 1);
		KeyFrame frame2 = new KeyFrame(Duration.seconds(time), keyValue2);

		timeline.getKeyFrames().addAll(frame1, frame2);
		timeline.play();
	}

	// used to animate a customer coming from "next customer" till the given queue
	/*
	 * private void movingCustomerAnimation(Node destination) { //HAS FLAWS, STILL
	 * UNDER DEVELOPMENT Bounds boundsInScene =
	 * destination.localToScene(destination.getBoundsInLocal());
	 * //System.out.println(boundsInScene.getMinX() + " " +
	 * boundsInScene.getMinY()); movingImage.setLayoutX(600);
	 * movingImage.setLayoutY(500); movingImage.setVisible(true);
	 * 
	 * Path path = new Path(); MoveTo moveTo = new MoveTo(0,0); QuadCurveTo
	 * quadCurve = new QuadCurveTo(boundsInScene.getMinX()-600
	 * ,boundsInScene.getMinY()-500, boundsInScene.getMinX() - 550,
	 * boundsInScene.getMinY() - 600);
	 * 
	 * path.getElements().add(moveTo); path.getElements().add(quadCurve);
	 * 
	 * PathTransition transition = new PathTransition();
	 * transition.setNode(movingImage); transition.setDuration(Duration.seconds(1));
	 * transition.setPath(path); transition.setCycleCount(1); transition.play();
	 * transition.setOnFinished(e -> movingImage.setVisible(false)); }
	 */

	// one thread that calculates when the next customer should come and direct it
	// to the proper queue
	private void getCustomers() {

		Thread getCustomers = new Thread(new Runnable() {

			@Override
			public void run() {
				while (endSimulation == false) {
					serviceTime = getRandomNumber(minService, maxService); // how much to add to queue ( service time of
																			// customer )
					int orderNr = MyQueue.getBestQueue(listqueue); // where to add to queue ( least total waiting time )
					listqueue.get(orderNr).addToQueue(serviceTime); // adding to queue

					Main.writeLogs("Adaug (automated) " + serviceTime);
					// System.out.print("adaug " + serviceTime);
					/*
					 * movingCustomerAnimation(progress.get(orderNr)); -- still UNDER DEVELOPMENT
					 * try { Thread.sleep(1000); }catch(InterruptedException e) {
					 * e.printStackTrace(); }
					 */
					// passing the queue index number, its size, and the service time for the head
					mainController.addCustomer(orderNr, listqueue.get(orderNr).getQueueSize(),
							listqueue.get(orderNr).getCurrentServiceTime(),
							listqueue.get(orderNr).getTotalWaitingTime()); // updating the GUI
					Main.writeLogs("Queue " + (orderNr + 1) + " totalWaitingTime = "
							+ listqueue.get(orderNr).getTotalWaitingTime());
					// System.out.println(listqueue.get(orderNr).getTotalWaitingTime());
					try {
						waitConsumer = getRandomNumber(minIncoming, maxIncoming); // wait time till the next customer
																					// comes
						progressNextCustomer(waitConsumer); // display to progressbar
						Main.writeLogs("Waiting " + waitConsumer + " seconds for next customer");
						// System.out.println("Waiting " + waitConsumer + " seconds for next
						// customer\n\n");
						Thread.sleep(waitConsumer * 1000); // wait till the next customer comes
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		});
		getCustomers.start();
	}

	public void removeQueue(MyQueue temp, int orderNr, ProgressBar startingPoint) {
		temp.removeFromQueue();
		mainController.removeCustomer(orderNr, temp.getQueueSize() + 1, listqueue.get(orderNr).getCurrentServiceTime(),
				temp.getTotalWaitingTime());
	}

	private void startSimulation(double time) { // stop all threads after the simulation time has passed
		Timer sim = new Timer(); // used to stop all threads after time is up
		TimerTask task = new TimerTask() {

			@Override
			public void run() {
				endSimulation = true;
				for (Threads temp : queueThreads) {
					temp.stopThread();
				}
				Main.writeLogs("Peak hour was reached after : " + (endTime-startTime)/1000000000 + " seconds, having " + peak + " customers");
			}
		};
		sim.schedule(task, (long) time * 1000);
	}

	public int getRandomNumber(int min, int max) {
		return new Random().nextInt((max - min) + 1) + min;
	}
}
