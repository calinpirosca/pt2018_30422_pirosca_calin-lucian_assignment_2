package Model;

import javafx.animation.Animation.Status;
import Controller.Simulation;
import application.Main;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.PathTransition;
import javafx.animation.Timeline;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.ImageView;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.util.Duration;

public class Threads extends Thread {
	private static ImageView movingImage;
	private static int count = 0;
	private static boolean endSimulation = false;
	private int id;
	private MyQueue queue;
	private Simulation simulation;
	private ProgressBar currentServicetime;
	PathTransition transition = new PathTransition();
	private double emptyQueueTime = 0;
	private long endTime, startTime;

	@Override
	public void run() {
		while (!endSimulation) { // wait for currentservice time and then removes the customer from queue
			if (queue.getQueueSize() > 0) {
				endTime = System.nanoTime();
				if (endTime > startTime && startTime != -1) {
					emptyQueueTime += endTime - startTime;
					startTime = -1;
				}
				this.currentServicetime.setVisible(true);
				try {
					queue.calculateAverageWaitingTime();
					this.startProgressBar(queue.getCurrentServiceTime());
					Thread.sleep(queue.getCurrentServiceTime() * 1000);
					removeAnimation(); // still to be tested
					simulation.removeQueue(queue, id, currentServicetime);
					//System.out.println(queue.getTotalWaitingTime());
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} else {
				this.currentServicetime.setVisible(false);
				if(endTime != -1) {
					startTime = System.nanoTime();
					endTime = -1;
				}
			}
		}
	}

	public void startProgressBar(double time) {
		Timeline timeline = new Timeline();
		KeyValue keyValue1 = new KeyValue(currentServicetime.progressProperty(), 0.01);
		KeyFrame frame1 = new KeyFrame(Duration.millis(0), keyValue1);

		KeyValue keyValue2 = new KeyValue(currentServicetime.progressProperty(), 1);
		KeyFrame frame2 = new KeyFrame(Duration.seconds(time), keyValue2);

		timeline.getKeyFrames().addAll(frame1, frame2);
		timeline.play();
	}

	public Threads(MyQueue temp, int i, Simulation main, Node currentServiceTime, ImageView movingImg) {
		simulation = main;
		this.queue = temp;
		this.id = count++;
		this.currentServicetime = (ProgressBar) currentServiceTime;
		this.currentServicetime.setVisible(false);
		movingImage = movingImg;
		// System.out.println("Am initializat threadu pentru queue " + count++);
		Main.writeLogs("Thread with id " + id + " was started");
	}

	public void stopThread() {
		if(queue.getQueueSize() == 0) {
			endTime = System.nanoTime();
			emptyQueueTime+=endTime - startTime;
		}
		while (queue.getQueueSize() > 0)
			;
		endSimulation = true;
		
		// System.out.println("am oprit threadu " + id);
		Main.writeLogs("Thread with id " + id + " was stopped");
		Main.writeLogs(queue.getAverageServiceTime() + " -- average service time for queue with id " + id + "\nEmpty Queue Time : " + emptyQueueTime/1000000000 + " secunde\naverage waiting time = " + queue.getAverageWaitingTime()/2);
	}

	public void removeAnimation() { // still under development, some minor flaws
		Bounds boundsInScene = currentServicetime.localToScene(currentServicetime.getBoundsInLocal());
		movingImage.setLayoutX(boundsInScene.getMinX() + 10);
		movingImage.setLayoutY(boundsInScene.getMinY() - 100);
		movingImage.setVisible(true);

		Path path = new Path();
		MoveTo moveTo = new MoveTo();
		moveTo.setX(0.0);
		moveTo.setY(0.0);

		LineTo lineTo = new LineTo(0, -400);

		path.getElements().add(moveTo);
		path.getElements().add(lineTo);

		if (transition.getStatus() == Status.STOPPED || transition.getStatus() == Status.PAUSED) {
			transition.setNode(movingImage);
			transition.setDuration(Duration.seconds(2));
			transition.setPath(path);
			transition.setCycleCount(1);
			transition.play();
		} else if (transition.getStatus() == Status.RUNNING) {
			transition.playFromStart();
		}
		transition.setOnFinished(e -> movingImage.setVisible(false));
	}
}