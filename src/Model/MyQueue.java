package Model;

import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

public class MyQueue {
	// I have multiple queues
	// each queue has an averagewaitingtime, the minimum averagewaitingtime and a
	// list of times, each one saying how much it'll take for that person in queue
	// to get it's problem done if it were single
	// averagewaitingtime = the average time it takes till the queue is empty,
	// considering only the current persons in the queue
	// not on overall waiting time since the start of the queue !!!!
	private int averageWaitingTime = 0;
	private int totalWaitingTime = 0;
	LinkedBlockingQueue<Integer> queue = new LinkedBlockingQueue<Integer>(); // intger represents the time it'll take to
																				// take 1 person in the queue.
	private int clients = 0;
	private int averageServiceTime = 0;
	// private int all persons, all sums -- to calculate overall average waiting
	// time

	
	// MODIFY, RETURN QUEUE NOT NUMBER !!
	public static int getBestQueue(List<MyQueue> list) {
		int min = Integer.MAX_VALUE, i = 0, orderNr = 0;
		for (MyQueue temp : list) {
			if (temp.getTotalWaitingTime() < min) {
				min = temp.getTotalWaitingTime();
				orderNr = i;
			}
			i++;
		}
		return orderNr;
	}
	
	public static int getTotalCustomers(List<MyQueue> list) { // return all the customers in the shop, used to calculate peak hour
		int customers = 0;
		for(MyQueue temp : list) {
			customers += temp.getQueueSize();
		}
		return customers;
	}

	public int getTotalWaitingTime() {
		return totalWaitingTime;
	}

	public double getAverageWaitingTime() {
		return (double)averageWaitingTime / clients;
	}

	public int getCurrentServiceTime() {
		if (queue.peek() == null) {
			return -1;
		} else {
			return queue.peek();
		}
	}
	
	public float getAverageServiceTime() {
		if(clients > 0) {
			return (float)averageServiceTime/clients;
		}else {
			return -1;
		}
	}
	
	public void removeFromQueue() { // pops the head of the queue
		int x = queue.poll();
		//totalWaitingTime = totalWaitingTime - x * (this.getQueueSize()+1); -- for average 
		totalWaitingTime -= x;
		calculateAverageWaitingTime(); // recalculate the current average waiting time
	}

	public void addToQueue(int wait) { // adds in the tail
		queue.add(wait);
		//totalWaitingTime = totalWaitingTime * 2 + wait; -- for average 
		totalWaitingTime += wait;
		calculateAverageWaitingTime(); // recalculate the current average waiting time
		clients++;
		averageServiceTime+= wait;
	}

	public void calculateAverageWaitingTime() {
		int i = queue.size();
		for (int temp : queue) {
			averageWaitingTime = averageWaitingTime + temp * i--;
		}
	}

	public int getQueueSize() {
		return queue.size();
	}
}
